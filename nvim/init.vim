set number
set expandtab
set tabstop=4
set shiftwidth=4
set nowrap

" Plugins
call plug#begin('~/.config/nvim/plugins')
Plug 'maralla/completor.vim'
call plug#end()

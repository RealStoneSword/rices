#!/bin/sh
CHECK_IF_RUNNING="$(pgrep droidcam)"
MPV_RUNNING="$(pgrep webcam | xargs -r -I {} pgrep -P {} mpv)"
usage() {
    cat << EOL
webcam - A script for easy management of droidcam
See README.md for more info

Usage:
    webcam help
    webcam start
    webcam start-here
    webcam start-network [IP]
    webcam status
    webcam view
    webcam stop

Options:
    help, --help    Shows the help
    start           Start droidcam with USB and fork to background
    start-here      Start droidcam with USB but without forking to background
    start-network   Start droid cam but use droidcam through the network (IP is needed)
    status          Checks if droidcam is running
    view            View droidcam using MPV
    stop            Stops droidcam
EOL
exit 0;
}
start() {
    if [ -n "$CHECK_IF_RUNNING" ]; then
        pkill droidcam
        echo "Droidcam already running, killing..."
    fi
    nohup droidcam-cli adb 4747 < /dev/null > /dev/null 2> /dev/null &
    exit 0;
}
start_here() {
    if [ -n "$CHECK_IF_RUNNING" ]; then
        echo "Droidcam already running, killing..."
        pkill droidcam
    fi
    droidcam-cli adb 4747
    exit 0;
}
start_network() {
    if [ -n "$CHECK_IF_RUNNING" ]; then
        echo "Droidcam already running, killing..."
        pkill droidcam
    fi
    nohup droidcam-cli "$1" "${DROIDCAM_PORT:-4747}" < /dev/null > /dev/null 2> /dev/null &
    exit 0;
}
status() {
    if [ -z "$CHECK_IF_RUNNING" ]; then
        echo "Droidcam is not running!"
        exit 1;
    else
        echo "Droidcam is running, run webcam view to view your camera with mpv";
        exit 0;
    fi
}
view() {
    if [ -z "$CHECK_IF_RUNNING" ]; then
        echo "Droidcam is not running!"
        exit 1;
    else
        mpv av://v4l2:/dev/video0 --profile=low-latency --untimed --no-osc
        exit 0;
    fi
}
stop() {
    if [ -z "$CHECK_IF_RUNNING" ]; then
        echo "Droidcam is not running!"
        exit 1;
    elif [ -n "$MPV_RUNNING" ]; then
        nohup kill "$MPV_RUNNING" < /dev/null > /dev/null 1> /dev/null 2> /dev/null &
    fi
    pkill droidcam
    exit 0;
}
[ "$1" = "--help" ] && usage
[ "$1" = "start" ] && start
[ "$1" = "start-here" ] && start_here
[ "$1" = "start-network" ] && start_network $2
[ "$1" = "status" ] && status
[ "$1" = "view" ] && view
[ "$1" = "stop" ] && stop
echo -e "webcam: missing or unknown operand\ntry --help"
exit 1;

typeset -U PATH path
path=("$path[@]" "${HOME}/Scripts")

# Start Pulseaudio
pulseaudio -D

[ "$(id -ru)" != 0 ] && [ "$(tty)" = "/dev/tty1" ] && startx

# Basic Stuff
HISTFILE=/home/ethan/.config/zsh/.zshistory
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
unsetopt beep
bindkey -e
zstyle :compinstall filename '/home/ethan/.config/zsh/.zshrc'
autoload -Uz compinit
compinit

# Prompt
source ~/.config/zsh/prompt.zsh

# Neofetch
neofetch

# Aliases
if [ -f ~/.config/zsh/.zsh_aliases ]; then
	. ~/.config/zsh/.zsh_aliases
fi

# Autosuggestions
source ~/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# Syntax Highlighting (MUST BE AT END OF FILE)
source ~/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
